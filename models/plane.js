const mongoose = require('mongoose')

const PlaneSchema = new mongoose.Schema({

  _id: { type: Number, required: true },
  company: {
    type: String,
    required: true,
    default: 'Boeing'
  },
  model: {
    type: Number,
    required: true,
    default: 747
  },
  year: {
    type: Number,
    required: true,
    default: 2000
  },
  seats: {
    type: Number,
    required: true,
    default: 1
  },
})
module.exports = mongoose.model('plane', PlaneSchema)
