const express = require('express')
const api = express.Router()

api.get('/FlightStats', (req, res) => {
    res.render('stats/FlightStats.ejs')
})
api.get('/PlaneStats', (req, res) => {
})

api.get('/PilotStats', (req, res) => {
res.render('stats/PilotStats.ejs')
})

module.exports = api 
